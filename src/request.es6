'use strict';

import http from 'http';
import Parser from './parse';

export default class StreamRequest {

    constructor() {
        this.parser = null;
    }

    /**
     * Set parser object.
     *
     * @param parser
     */
    setParser(parser) {
        this.parser = parser || new Parser();
    }

    /**
     * Connect and process stream.
     *
     * @param url
     */
    connect(url) {
        let parser = this.parser;

        let req = http.request(url, function (request) {

            parser.parseRequestInfo(request.headers);
            request.pipe(parser);
        });
        req.setHeader('Icy-MetaData', 1);
        req.end();
    }

    /**
     * Event change track.
     *
     * @param cb
     */
    eventChangeTrack(cb) {
        this.parser.on('changeTrack', cb.bind(this.parser));
    }

    /**
     * Event process chunk.
     *
     * @param cb
     */
    eventProccessPackage(cb) {
        this.parser.on('afterChunk', cb.bind(this.parser));
    }
}