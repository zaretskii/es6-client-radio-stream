'use strict';
require('stream');
import {Transform} from 'stream';
import events from 'events';
import _ from 'lodash';

export default class Parse extends Transform {

    constructor(options) {

        super(options);
        this.sysInfo = {
            metaIntTag: 'icy-metaint',
            encoding: 'utf-8',
            events: ['changeTrack', 'beforeChunk', 'afterChunk']
        };
        this.streamInfo = {
            mainStream: new Buffer(0),
            info: {
                title: null,
                pkgs: 0
            }

        };
        this.reqInfo = null;
        // event emitter.
        _.merge(this, events.EventEmitter());
    }

    _transform(chunk, encode, done) {
        //console.log(this.streamInfo.mainStream.length);
        let buff = new Buffer(chunk, encode);
        this.emit('beforeChunk', buff);
        // process data.
        this._parseMainStream(buff);
        this.emit('afterChunk', buff);

        done();
    }

    /**
     * Process chunk.
     *
     * @param mainBuffer
     * @private
     */
    _parseMainStream(mainBuffer) {
        this.streamInfo.info.pkgs++;
        this.streamInfo.mainStream = (!this.streamInfo.mainStream.length)?
            mainBuffer : Buffer.concat([this.streamInfo.mainStream, mainBuffer]);
        while (this.streamInfo.mainStream.length > this._metaInt()) {
            // init stream.
            let mainStream = this.streamInfo.mainStream;
            let metaSize = mainStream[this._metaInt()] * 16;
            let meta = mainStream.slice(this._metaInt());
            // process data.
            let title = meta.toString(this.sysInfo.encoding, 1, metaSize + 1);
            if (title !=='' && title !== this.streamInfo.info.title) {
                this._changeTrack(title);
            }
            this._flushMainStream(metaSize);
        }
    }

    /**
     * Return metadata int.
     *
     * @returns {number}
     * @private
     */
    _metaInt() {
        return _.parseInt(this.reqInfo[this.sysInfo.metaIntTag]);
    }

    /**
     * Flush main stream.
     *
     * @param metaSize
     * @private
     */
    _flushMainStream(metaSize) {
        this.streamInfo.mainStream = this.streamInfo.mainStream.slice(this._metaInt() + metaSize + 1);
    }

    /**
     * Event change track.
     *
     * @param title
     * @private
     */
    _changeTrack(title) {
        this.streamInfo.info.title = title;
        this.emit('changeTrack', title);
    }

    /**
     * Return events list.
     *
     * @returns {Array}
     */
    events() {
        return this.sysInfo.events;
    }

    /**
     * Process request header.
     *
     * @param reqHeader
     */
    parseRequestInfo(reqHeader) {
        this.reqInfo = reqHeader;
    }

    /**
     * Return count processed packages.
     */
    countPackages() {
        return this.streamInfo.info.pkgs;
    }

    /**
     * Return active title.
     *
     * @returns {null}
     */
    title() {
        return this.streamInfo.info.title;
    }
}