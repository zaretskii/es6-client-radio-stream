'use strict';

import RadioRequest from './request';

export default class Index {

	constructor (url) {

        this.request = new RadioRequest(url);
        this.request.setParser();
        this.request.eventChangeTrack(function (title) {
            console.log("\n", title, "\n");
        });

        this.request.connect(url);
	}
};